package topbanner;

/**
 * Created by SEO on 2016-01-06.
 */
public class FoodListBannerCallBackItem {
    private Result result;

    public Result getResult ()
    {
        return result;
    }

    public void setResult (Result result)
    {
        this.result = result;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [result = "+result+"]";
    }

    public class Result
    {
        private String state;

        private Data[] data;

        public String getState ()
        {
            return state;
        }

        public void setState (String state)
        {
            this.state = state;
        }

        public Data[] getData ()
        {
            return data;
        }

        public void setData (Data[] data)
        {
            this.data = data;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [state = "+state+", data = "+data+"]";
        }
    }
    public class Data
    {
        private String user_name;

        private Food_data food_data;

        public String getUser_name ()
        {
            return user_name;
        }

        public void setUser_name (String user_name)
        {
            this.user_name = user_name;
        }

        public Food_data getFood_data ()
        {
            return food_data;
        }

        public void setFood_data (Food_data food_data)
        {
            this.food_data = food_data;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [user_name = "+user_name+", food_data = "+food_data+"]";
        }
    }
    public class Food_data
    {
        private Dinner[] dinner;

        private Lunch[] lunch;

        private Morning[] morning;

        public Dinner[] getDinner ()
        {
            return dinner;
        }

        public void setDinner (Dinner[] dinner)
        {
            this.dinner = dinner;
        }

        public Lunch[] getLunch ()
        {
            return lunch;
        }

        public void setLunch (Lunch[] lunch)
        {
            this.lunch = lunch;
        }

        public Morning[] getMorning ()
        {
            return morning;
        }

        public void setMorning (Morning[] morning)
        {
            this.morning = morning;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [dinner = "+dinner+", lunch = "+lunch+", morning = "+morning+"]";
        }
    }
    public class Dinner
    {
        private String food_name;

        private String kcal;

        public String getFood_name ()
        {
            return food_name;
        }

        public void setFood_name (String food_name)
        {
            this.food_name = food_name;
        }

        public String getKcal ()
        {
            return kcal;
        }

        public void setKcal (String kcal)
        {
            this.kcal = kcal;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [food_name = "+food_name+", kcal = "+kcal+"]";
        }
    }
    public class Lunch
    {
        private String food_name;

        private String kcal;

        public String getFood_name ()
        {
            return food_name;
        }

        public void setFood_name (String food_name)
        {
            this.food_name = food_name;
        }

        public String getKcal ()
        {
            return kcal;
        }

        public void setKcal (String kcal)
        {
            this.kcal = kcal;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [food_name = "+food_name+", kcal = "+kcal+"]";
        }
    }
    public class Morning
    {
        private String food_name;

        private String kcal;

        public String getFood_name ()
        {
            return food_name;
        }

        public void setFood_name (String food_name)
        {
            this.food_name = food_name;
        }

        public String getKcal ()
        {
            return kcal;
        }

        public void setKcal (String kcal)
        {
            this.kcal = kcal;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [food_name = "+food_name+", kcal = "+kcal+"]";
        }
    }

}
