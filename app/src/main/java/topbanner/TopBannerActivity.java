package topbanner;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.marusoft.marusoft_sc.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;
import scjcdb.SCDBManager;
import scjcdb.SCWannaWeight;
import scjcdb.SC_FOOD;
import scjcdb.SC_FoodCategory;
import scjcdb.UploadFoodItemManager;

/**
 * Created by Yoon on 2016-01-05.
 */
public class TopBannerActivity extends Activity{
    Gson gson;
    RestAdapter restAdapter;
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.topbanner);
        textView= (TextView)findViewById(R.id.textView);


        initGsonDefault();
        initRestAdpaterDefault();


        UploadFoodItemManager uploadFoodItemManager = new UploadFoodItemManager();

        SCDBManager scdbManager = new SCDBManager(TopBannerActivity.this);
        SCWannaWeight wannaWeight =    scdbManager.getWannaWeight(170, SCWannaWeight.MAN_ID);

        ArrayList<SCWannaWeight> scWannaWeightArrayList =scdbManager.getAllWannaTable(SCWannaWeight.MAN_ID);
        for(int i = 0  ; i <scWannaWeightArrayList.size();i++){
            Log.v("henen",scWannaWeightArrayList.get(i).toString());
        }
        Log.v("henen", wannaWeight.toString());
        ArrayList<SC_FoodCategory>sc_foodCategories =  scdbManager.getSCFoodCategory();
        for(int i = 0 ; i <sc_foodCategories.size();i++){
            Log.v("henen", sc_foodCategories.get(i).toString());
            ArrayList<SC_FOOD> sc_foodArrayList =  scdbManager.getFood(sc_foodCategories.get(i));

        for(int j = 0, k =0 ; j<sc_foodArrayList.size();j++,k++){
            if(k%3==0){
                uploadFoodItemManager.morningAdd(sc_foodArrayList.get(j));
            }
            if(k%3==1){
                uploadFoodItemManager.lunch_arrayList(sc_foodArrayList.get(j));
            }
            if(k%3==2){
                uploadFoodItemManager.dinner_arrayList(sc_foodArrayList.get(j));
            }

            Log.v("henen2", sc_foodArrayList.get(j).toString());
        }

    }




//아침 점심 저녁 업로드
/*
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject =  uploadFoodItemManager.makeJson();
        restAdapter.create(MarusoftApiService.class).uploadFoodList("1234", jsonObject.toString(), new Callback<String>() {
            @Override
            public void success(String s, Response response) {
                    Log.v("henen", s);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
*/
        //탑 배너
        restAdapter.create(MarusoftApiService.class).getRecentFoodList(3, new Callback<FoodListBannerCallBackItem>() {
            @Override
            public void success(FoodListBannerCallBackItem foodListBannerCallBackItem, Response response) {
                String temp  = "";
                for(int i = 0 ;  i <foodListBannerCallBackItem.getResult().getData().length;i++){
                    temp  = temp + foodListBannerCallBackItem.getResult().getData()[i].getUser_name() + " : ";
                    for(int j = 0 ; j<foodListBannerCallBackItem.getResult().getData()[i].getFood_data().getMorning().length;j++){
                        temp  = temp+ foodListBannerCallBackItem.getResult().getData()[i].getFood_data().getMorning()[j].getFood_name() + ",";
                        temp  = temp+ foodListBannerCallBackItem.getResult().getData()[i].getFood_data().getMorning()[j].getKcal() + "";
                    }
                    for(int j = 0 ; j<foodListBannerCallBackItem.getResult().getData()[i].getFood_data().getLunch().length;j++){
                        temp  = temp+ foodListBannerCallBackItem.getResult().getData()[i].getFood_data().getLunch()[j].getFood_name() + ",";
                        temp  = temp+ foodListBannerCallBackItem.getResult().getData()[i].getFood_data().getLunch()[j].getKcal() + "";
                    }
                    for(int j = 0 ; j<foodListBannerCallBackItem.getResult().getData()[i].getFood_data().getDinner().length;j++){
                        temp  = temp+ foodListBannerCallBackItem.getResult().getData()[i].getFood_data().getDinner()[j].getFood_name() + ",";
                        temp  = temp+ foodListBannerCallBackItem.getResult().getData()[i].getFood_data().getDinner()[j].getKcal() + "";
                    }

                }

                textView.setText(temp);
                textView.setSelected(true);
            }



            @Override
            public void failure(RetrofitError error) {

            }
        });


        /*
이메일보내기

         */

        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject =  uploadFoodItemManager.makeJson();
        restAdapter.create(MarusoftApiService.class).shareEmail("yss1530@gmail.com", jsonObject.toString(), new Callback<String>() {
            @Override
            public void success(String s, Response response) {
                Log.v("henen", s);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    protected void initGsonDefault(){
        this.gson = new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return f.getDeclaringClass().equals(Date.class);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .create();
    }

    protected void initRestAdpaterDefault(){
        /**
         * 레트로핏 설정
         */
        this.restAdapter = new RestAdapter.Builder()
                //로그 레벨 설정
                .setLogLevel(RestAdapter.LogLevel.FULL)
                        //BASE_URL 설정
                .setEndpoint(MarusoftApiService.API_URL)
                        //Gson Converter 설정
                .setConverter(new GsonConverter(gson))
                .build();
    }
}
