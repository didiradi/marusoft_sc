package topbanner;

import net.marusoft.marusoft_sc.HistoryItem;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;

/**
 * Created by Henen on 2016-01-05.
 */
public interface MarusoftApiService {
    public final static String API_URL  = "http://cloud.marusoft.net/api/sunchang";

    @FormUrlEncoded
    @POST("/uploadFoodList.php")
    void uploadFoodList(@Field("user_name") String user_name, @Field("json") String json, Callback<String> callback);


    @FormUrlEncoded
    @POST("/getRecentFoodList.php")
    void getRecentFoodList(@Field("count") int count, Callback<FoodListBannerCallBackItem> callback);



    @FormUrlEncoded
    @POST("/getUserHistory.php")
    void getUserHistory(@Field("start_index") int start_index, @Field("count") int count, Callback<HistoryItem> callback);



    @FormUrlEncoded
    @POST("/shareEmail.php")
    void shareEmail(@Field("email") String email, @Field("json") String json, Callback<String> callback);

}
