package net.marusoft.marusoft_sc;

import java.util.ArrayList;

public class HistoryItem
{

    public static final String STATUS_OK = "OK";
    public static final String STATUS_NO= "NO";

    private Data data;

    private Meta meta;

    public Data getData ()
    {
        return data;
    }

    public void setData (Data data)
    {
        this.data = data;
    }

    public Meta getMeta ()
    {
        return meta;
    }

    public void setMeta (Meta meta)
    {
        this.meta = meta;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [data = "+data+", meta = "+meta+"]";
    }
    public class Data
    {
        private String startIndex;

        private ArrayList<Items> items;

        private String currentItemCount;

        private String itemsPerPage;

        public String getStartIndex ()
        {
            return startIndex;
        }

        public void setStartIndex (String startIndex)
        {
            this.startIndex = startIndex;
        }

        public ArrayList<Items> getItems() {
            return items;
        }

        public void setItems(ArrayList<Items> items) {
            this.items = items;
        }

        public String getCurrentItemCount ()
        {
            return currentItemCount;
        }

        public void setCurrentItemCount (String currentItemCount)
        {
            this.currentItemCount = currentItemCount;
        }

        public String getItemsPerPage ()
        {
            return itemsPerPage;
        }

        public void setItemsPerPage (String itemsPerPage)
        {
            this.itemsPerPage = itemsPerPage;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [startIndex = "+startIndex+", items = "+items+", currentItemCount = "+currentItemCount+", itemsPerPage = "+itemsPerPage+"]";
        }
    }

    public class Items
    {
        private String id;

        private String user_name;

        public String getId ()
        {
            return id;
        }

        public void setId (String id)
        {
            this.id = id;
        }

        public String getUser_name ()
        {
            return user_name;
        }

        public void setUser_name (String user_name)
        {
            this.user_name = user_name;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [id = "+id+", user_name = "+user_name+"]";
        }
    }
    public class Meta
    {
        private String message;

        private String status;

        private String code;

        private String type;

        public String getMessage ()
        {
            return message;
        }

        public void setMessage (String message)
        {
            this.message = message;
        }

        public String getStatus ()
        {
            return status;
        }

        public void setStatus (String status)
        {
            this.status = status;
        }

        public String getCode ()
        {
            return code;
        }

        public void setCode (String code)
        {
            this.code = code;
        }

        public String getType ()
        {
            return type;
        }

        public void setType (String type)
        {
            this.type = type;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [message = "+message+", status = "+status+", code = "+code+", type = "+type+"]";
        }
    }
}
