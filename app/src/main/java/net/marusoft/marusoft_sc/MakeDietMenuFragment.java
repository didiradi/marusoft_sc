package net.marusoft.marusoft_sc;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import scjcdb.SC_FOOD;

/**
 * Created by SEO on 2016-01-07.
 */
public class MakeDietMenuFragment extends Fragment {
    private static final String ARG_TITLE = "title";
    private String param_title;



    GridView gridView;

    private int RICE = 0, SOUP = 1, SIDE1 = 2, SIDE2 = 3, SIDE3 = 4, ONEDISH = 5;


    private String category;
    public static Context context;
    private int seletedMenu;


    //Activity의 함수를 호출하기 위한 리스너
    OnClickToFoodImageOnTableListener onClickToFoodImageOnTalbeListen;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param_title Parameter 1.
     * @return A new instance of fragment QuizFragment.
     */
    public MakeDietMenuFragment newInstance(String param_title){
        MakeDietMenuFragment makeDietMenuFragment = new MakeDietMenuFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TITLE, param_title);
        makeDietMenuFragment.setArguments(args);
        return makeDietMenuFragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            param_title = getArguments().getString(ARG_TITLE);
            this.onClickToFoodImageOnTalbeListen = (OnClickToFoodImageOnTableListener)getActivity();
        }



    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.from(getActivity()).inflate(R.layout.activity_make_dietmenu_formine,null);
        gridView = (GridView)view.findViewById(R.id.foodmenugridview);
        gridView.setAdapter(new GridAdapter());
        return view;
    }

    public class GridAdapter extends BaseAdapter {
        LayoutInflater inflater;
        ViewHolder viewHolder;
        public GridAdapter(){
            inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public final int getCount(){
            return 6;
        }

        public final Object getItem(int position){
            return position;
        }

        public final long getItemId(int position){
            return position;
        }

        public View getView(final int position, View convertView,ViewGroup parent){

            if(convertView ==null){
                viewHolder = new ViewHolder();
                convertView = inflater.inflate(R.layout.foodmenuitem,parent,false);
                viewHolder.categoryTextView = (TextView) convertView.findViewById(R.id.foodcategory);
                viewHolder.foodimageView = (ImageView) convertView.findViewById(R.id.foodmenugridImg);
                viewHolder.foodnameTextView = (TextView) convertView.findViewById(R.id.foodnamegridmenu);
                convertView.setTag(viewHolder);
            }else{
                viewHolder  = (ViewHolder)convertView.getTag();
            }


            if(position==RICE){
                viewHolder.categoryTextView.setText("밥");
            }else if(position==SOUP){
                viewHolder.categoryTextView.setText("국");
            }else if(position==SIDE1 || position ==SIDE2 || position ==SIDE3){
                viewHolder.categoryTextView.setText("반찬");
            }else{
                viewHolder.categoryTextView.setText("일품");
            }


            viewHolder.foodimageView.setOnClickListener(new Button.OnClickListener(){
                public void onClick(View v){
                    seletedMenu = position;
                    //new SlideOutUnderneathAnimation(testimg).setDirection(
                    //Animation.DIRECTION_DOWN).animate();
                    if(seletedMenu==RICE){
                        category = "f1";
                        onClickToFoodImageOnTalbeListen.onClickToFoodImageOnTable(category);

                    }else if(seletedMenu==SOUP){

                        category = "f2";
                        onClickToFoodImageOnTalbeListen.onClickToFoodImageOnTable(category);
                    }else if(seletedMenu==SIDE1 || seletedMenu ==SIDE2 || seletedMenu ==SIDE3){

                        category = "f3";
                        onClickToFoodImageOnTalbeListen.onClickToFoodImageOnTable(category);
                    }else{

                        category = "f8";
                        onClickToFoodImageOnTalbeListen.onClickToFoodImageOnTable(category);
                    }
                }
            });



            return convertView;
        }

        public class ViewHolder{
            public  TextView categoryTextView;
            public  ImageView foodimageView ;
            public  TextView foodnameTextView;
        }
    }


    /*
     여기 fragment에서 activity의 원하는 함수를 호출하려면 여기에 정의하고
     activity에서 내용을 구현한 다음
     위의  OnClickToFoodImageOnTableListener onClickToFoodImageOnTalbeListen;
     을 이용하여 접근, 호출하면 된다.
      */
    public interface  OnClickToFoodImageOnTableListener{
        public void onClickToFoodImageOnTable(String category);
        public void WhichFoodSeletedChecked(int selectedMenu);
    }


    //리사이클 뷰에서 어떤 해당 메뉴 선택시 그리드뷰안에 있는 음식메뉴의 이미지와 이름을 바꿔주기 위해 호출되는 함수
    public void setFoodImageOnTable(SC_FOOD seletedFood) {
        // 이미지와 텍스트를 바꿔 주는 곳
        //
        ViewGroup gridChild = (ViewGroup) gridView.getChildAt(seletedMenu);
        ViewGroup gridImageRelativeLayout = (ViewGroup)gridChild.getChildAt(0);

        ((ImageView) gridImageRelativeLayout.getChildAt(0)).setImageResource(R.drawable.testimage);
        ((TextView) gridChild.getChildAt(1)).setText(seletedFood.getName());

        onClickToFoodImageOnTalbeListen.WhichFoodSeletedChecked(seletedMenu);

                /* 애니매이션 주는 법
                onefoodimg.setImageResource(R.drawable.testimage);
                onedishtxt.setText(seletedFood.getName());
                new SlideInUnderneathAnimation(onefoodimg).setDirection(
                        Animation.DIRECTION_DOWN).animate();
                */
    }

}
