/*
 * Copyright (C) 2015 Two Toasters
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.marusoft.marusoft_sc;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import scjcdb.SC_FOOD;

class SampleRecyclerViewAdapter extends Adapter<SampleRecyclerViewAdapter.SampleViewHolder> {

    private ArrayList<SC_FOOD> items;
    private int itemLayoutRes;
    private  Context context;
    private MakeDietMenuFragment makeDietMenuFragment;



    SampleRecyclerViewAdapter(ArrayList<SC_FOOD> items, int itemLayoutRes,MakeDietMenuFragment makeDietMenuFragment) {
        this.items = items;//Arrays.asList(items);
        this.itemLayoutRes = itemLayoutRes;
        this.makeDietMenuFragment =makeDietMenuFragment;
        //this.context = context;
    }

    SampleRecyclerViewAdapter(ArrayList<SC_FOOD> items, int itemLayoutRes) {
        this.items = items;//Arrays.asList(items);
        this.itemLayoutRes = itemLayoutRes;
        //this.context = context;
    }



    @Override
    public SampleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view;
        view = inflater.inflate(itemLayoutRes, parent, false);
        return new SampleViewHolder(view);
    }


    //리사이클뷰에 각 해당 칸이 터치 되었을때 실행되는 곳
    @Override
    public void onBindViewHolder(SampleViewHolder holder, final int position) {
        holder.linear.setBackgroundColor(backgroundColor(position, holder));
        //각 반찬과 음식에 맞는 것을 여기 넣어줘야함.
        holder.img.setImageResource(R.drawable.testimage);
        holder.textView.setText(items.get(position).getName());

        holder.view.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {

                makeDietMenuFragment.setFoodImageOnTable(items.get(position));

            }
        });
    }

    @Override
    public int getItemCount() {
        return items != null ? items.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        return  0;
    }

    private int backgroundColor(int position, SampleViewHolder holder) {

            return holder.itemView.getResources().getColor(position % 2 == 0 ? R.color.even : R.color.odd);

    }

    public static class SampleViewHolder extends RecyclerView.ViewHolder {
        final LinearLayout linear;
        final ImageView img;
        final TextView textView;
        View view;
        SampleViewHolder(View view) {

            super(view);
            this.view  = view;
            linear = (LinearLayout)view.findViewById(R.id.text);
            img = (ImageView) view.findViewById(R.id.foodimgInList);
            textView = (TextView)view.findViewById(R.id.foodNameInList);
        }
    }


}
