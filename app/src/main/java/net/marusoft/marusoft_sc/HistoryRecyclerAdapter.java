package net.marusoft.marusoft_sc;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by SEO on 2016-01-27.
 */
public class HistoryRecyclerAdapter extends RecyclerView.Adapter<HistoryRecyclerAdapter.ViewHolder> {
    private HistoryItem items;
    private Context context;
    private int item_layout;
    public HistoryRecyclerAdapter(Context context, HistoryItem items, int item_layout) {
        this.context=context;
        this.items=items;
        this.item_layout=item_layout;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.history_item, null);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Log.v("item", "item : " + items.toString());
        holder.name_textview.setText(items.getData().getItems().get(position).getUser_name() + " 님의");
    }

    @Override
    public int getItemCount() {
        return items.getData().getItems().size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        protected View view;
        TextView name_textview;

        public ViewHolder(View view) {
            super(view);
            this.view = view;
            name_textview = (TextView) view.findViewById(R.id.name_textview);
        }
    }
}
