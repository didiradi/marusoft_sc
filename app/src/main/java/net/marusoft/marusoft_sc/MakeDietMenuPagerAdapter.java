package net.marusoft.marusoft_sc;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;


/**
 * Created by SEO on 2016-01-07.
 */
public class MakeDietMenuPagerAdapter extends FragmentStatePagerAdapter {
    public static final int NUM_PAGES = 3;
    public MakeDietMenuFragment morning_fragment;
    public MakeDietMenuFragment lunch_fragment;
    public MakeDietMenuFragment dinner_fragment;
    public MakeDietMenuPagerAdapter(FragmentManager fm) {
        super(fm);
        this.morning_fragment =  new MakeDietMenuFragment().newInstance("morning");
        this.lunch_fragment =  new MakeDietMenuFragment().newInstance("lunch");
        this.dinner_fragment =  new MakeDietMenuFragment().newInstance("dinner");
    }
    @Override
    public Fragment getItem(int position) {

        if(position == 0) {
            return morning_fragment;
        } else if(position == 1) {
            return lunch_fragment;
        } else{
            return dinner_fragment;
        }
    }

    @Override
    public int getCount() {
        return NUM_PAGES;
    }


    @Override
    public CharSequence getPageTitle(int position) {
        if(position == 0) {
            return "아침";
        } else if(position == 1) {
            return "점심";
        } else{
            return "저녁";
        }
    }
}

