package net.marusoft.marusoft_sc;

/**
 * Created by MARUSOFT-CHOI on 2016-01-30.
 */

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Date;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;
import topbanner.MarusoftApiService;
import util.EndlessRecyclerOnScrollListener;

public class HistoryActivity extends Activity {
    public final static int MORE_INDEX = 20;
    int start_index = 0;
    RecyclerView ht_recycleView;
    Gson gson;
    RestAdapter restAdapter;
    HistoryRecyclerAdapter adapter;
    HistoryItem item;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history_activity);
        context = this;
        initGsonDefault();
        initRestAdpaterDefault();
        ht_recycleView = (RecyclerView) findViewById(R.id.ht_recycleView);

        GridLayoutManager layoutManager = new GridLayoutManager(context,2);
        layoutManager.setOrientation(GridLayoutManager.VERTICAL);
        ht_recycleView.setLayoutManager(layoutManager);
        ht_recycleView.setOnScrollListener(new EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                Log.v("들어옴", "들어옴");
                gethistory_data();
            }
        });
        Log.v("dd", "dd");
        gethistory_data();
    }

    void gethistory_data() {
        restAdapter.create(MarusoftApiService.class).getUserHistory(start_index, MORE_INDEX, new Callback<HistoryItem>() {
            @Override
            public void success(HistoryItem historyItem, Response response) {
                if(historyItem.getMeta().getStatus().equals(HistoryItem.STATUS_OK)) {
                    if(start_index == 0) {
                        item = historyItem;
                        adapter = new HistoryRecyclerAdapter(context, historyItem, R.layout.history_activity);
                        ht_recycleView.setAdapter(adapter);
                    } else {
                        item.getData().getItems().addAll(historyItem.getData().getItems());
                        adapter.notifyDataSetChanged();
                    }
                    start_index += MORE_INDEX;
                    // Log.v("HistoryActivitiy", historyItem.getData().getItems().get(0).getUser_name());

                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }
    protected void initGsonDefault(){
        this.gson = new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return f.getDeclaringClass().equals(Date.class);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .create();
    }

    protected void initRestAdpaterDefault(){
        /**
         * 레트로핏 설정
         */
        this.restAdapter = new RestAdapter.Builder()
                //로그 레벨 설정
                .setLogLevel(RestAdapter.LogLevel.FULL)
                        //BASE_URL 설정
                .setEndpoint(MarusoftApiService.API_URL)
                        //Gson Converter 설정
                .setConverter(new GsonConverter(gson))
                .build();
    }

}

