package net.marusoft.marusoft_sc;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;

/**
 * Created by MARUSOFT-CHOI on 2016-01-05.
 */

public class SplashActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);

               Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                //Intent intent = new Intent(SplashActivity.this, MakeDietmenuFormineActivity.class);
                startActivity(intent);
                finish();
            }
        };
        Log.v("","");
        handler.sendEmptyMessageDelayed(0, 2000);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }
}

