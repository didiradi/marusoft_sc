package net.marusoft.marusoft_sc;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.twotoasters.jazzylistview.JazzyHelper;
import com.twotoasters.jazzylistview.recyclerview.JazzyRecyclerViewScrollListener;

import java.sql.Date;
import java.util.ArrayList;

import model.DietMenu;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;
import scjcdb.SCDBManager;
import scjcdb.SCWannaWeight;
import scjcdb.SC_FOOD;
import scjcdb.SC_FoodCategory;
import topbanner.FoodListBannerCallBackItem;
import topbanner.MarusoftApiService;


public class MakeDietmenuFormineActivity extends FragmentActivity implements MakeDietMenuFragment.OnClickToFoodImageOnTableListener{

    public static ViewPager mViewPager;
    MakeDietMenuPagerAdapter makeDietMenuPagerAdapter;
    private TabLayout tabLayout;

    private DietMenu dietMenuMorning;
    private DietMenu dietMenuLunch;
    private DietMenu dietMenuNight;

    Gson gson;
    RestAdapter restAdapter;


    ImageButton saveButton;
    ImageButton shareButton;
    ImageButton endButton;


    //recycleview 사용하기
    private int RICE = 0, SOUP = 1, SIDE1 = 2, SIDE2 = 3, SIDE3 = 4, ONEDISH = 5;

    private int mCurrentTransitionEffect = JazzyHelper.FADE;
    private JazzyRecyclerViewScrollListener jazzyScrollListener;
    private RecyclerView recyclerView;
    private SCDBManager scdbManager;
    private SCWannaWeight wannaWeight;
    private ArrayList<SCWannaWeight> scWannaWeightArrayList;
    private ArrayList<SC_FoodCategory>sc_foodCategories;
    private ArrayList<SC_FOOD> sc_foodArrayList;
    int itemLayoutRes;

    //어떤 fragment인지 구분하여 서로 통신을 위한 플래그
    String seletedTime = "morning";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_dietmenu_activity);

        dietMenuMorning = new DietMenu("morning");
        dietMenuLunch = new DietMenu("lunch");
        dietMenuNight = new DietMenu("night");

        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        makeDietMenuPagerAdapter=  new MakeDietMenuPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(makeDietMenuPagerAdapter);
        mViewPager.setOffscreenPageLimit(3); // page 확정



        initGsonDefault();
        initRestAdpaterDefault();


        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            ArrayList<SC_FOOD> temp = new ArrayList<SC_FOOD>();
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(tab.getPosition()==0){
                    seletedTime = "morning";
                }else if(tab.getPosition()==1){
                    seletedTime="lunch";
                }else{
                    seletedTime="night";
                }
                recyclerView.setAdapter(new SampleRecyclerViewAdapter(temp, itemLayoutRes)); // 리사이클뷰를 리셋하기 위해서
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        setupTabIcons();
/*

        //탑 배너
        restAdapter.create(MarusoftApiService.class).getRecentFoodList(3, new Callback<FoodListBannerCallBackItem>() {
            @Override
            public void success(FoodListBannerCallBackItem foodListBannerCallBackItem, Response response) {
                String temp = "";
                for (int i = 0; i < foodListBannerCallBackItem.getResult().getData().length; i++) {
                    temp = temp + foodListBannerCallBackItem.getResult().getData()[i].getUser_name() + " : ";
                    for (int j = 0; j < foodListBannerCallBackItem.getResult().getData()[i].getFood_data().getMorning().length; j++) {
                        temp = temp + foodListBannerCallBackItem.getResult().getData()[i].getFood_data().getMorning()[j].getFood_name() + ",";
                        temp = temp + foodListBannerCallBackItem.getResult().getData()[i].getFood_data().getMorning()[j].getKcal() + "";
                    }
                    for (int j = 0; j < foodListBannerCallBackItem.getResult().getData()[i].getFood_data().getLunch().length; j++) {
                        temp = temp + foodListBannerCallBackItem.getResult().getData()[i].getFood_data().getLunch()[j].getFood_name() + ",";
                        temp = temp + foodListBannerCallBackItem.getResult().getData()[i].getFood_data().getLunch()[j].getKcal() + "";
                    }
                    for (int j = 0; j < foodListBannerCallBackItem.getResult().getData()[i].getFood_data().getDinner().length; j++) {
                        temp = temp + foodListBannerCallBackItem.getResult().getData()[i].getFood_data().getDinner()[j].getFood_name() + ",";
                        temp = temp + foodListBannerCallBackItem.getResult().getData()[i].getFood_data().getDinner()[j].getKcal() + "";
                    }

                }


            }


            @Override
            public void failure(RetrofitError error) {

            }
        });
*/

        //recycleview 사용
        scdbManager = new SCDBManager(this);
        wannaWeight = scdbManager.getWannaWeight(170, SCWannaWeight.MAN_ID);
        scWannaWeightArrayList = scdbManager.getAllWannaTable(SCWannaWeight.MAN_ID);
        sc_foodCategories =  scdbManager.getSCFoodCategory();

        for(int i = 0 ; i <sc_foodCategories.size();i++){
            Log.v("henen", sc_foodCategories.get(i).toString());
            ArrayList<SC_FOOD> sc_foodArrayList =  scdbManager.getFood(sc_foodCategories.get(i));
            for(int j = 0 ; j<sc_foodArrayList.size();j++){
                Log.v("henen2", sc_foodArrayList.get(j).toString());
            }
        }

        ArrayList<SC_FOOD> temp = new ArrayList<SC_FOOD>();
        itemLayoutRes = R.layout.food_list_in_recycleview_item; // 리사이클뷰의 각각의 해당 네모칸임

        recyclerView = (RecyclerView)findViewById(R.id.foodlist);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(new SampleRecyclerViewAdapter(temp, itemLayoutRes));// 여기가 넣는 부분이다.

        jazzyScrollListener = new JazzyRecyclerViewScrollListener();
        recyclerView.setOnScrollListener(jazzyScrollListener);
        jazzyScrollListener.setTransitionEffect(mCurrentTransitionEffect);


        //밑에 버튼들 save share end button

        saveButton = (ImageButton)findViewById(R.id.savebutton);
        shareButton= (ImageButton)findViewById(R.id.sharebutton);
        endButton= (ImageButton)findViewById(R.id.endbutton);




        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAllMenu();
            }
        });

        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert_confirm = new AlertDialog.Builder(MakeDietmenuFormineActivity.this);
                final EditText input = new EditText(MakeDietmenuFormineActivity.this);
                alert_confirm.setView(input);

                alert_confirm.setMessage("이메일을 입력해주세요").setCancelable(false).setPositiveButton("확인",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                checkAllMenu();
                                String email = input.getText().toString();
                                Log.d(email, email);

                                //이메일 보낼시 써줘야 하는 곳
                                // 'YES'
                            }
                        }).setNegativeButton("취소",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                return;
                            }
                        });
                AlertDialog alert = alert_confirm.create();
                alert.show();
            }
        });

        endButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }
    private void setupTabIcons(){

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_make_dietmenu_formine, menu);
        return true;
    }


    protected void initGsonDefault(){
        this.gson = new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return f.getDeclaringClass().equals(Date.class);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .create();
    }

    protected void initRestAdpaterDefault(){
        /**
         * 레트로핏 설정
         */
        this.restAdapter = new RestAdapter.Builder()
                //로그 레벨 설정
                .setLogLevel(RestAdapter.LogLevel.FULL)
                        //BASE_URL 설정
                .setEndpoint(MarusoftApiService.API_URL)
                        //Gson Converter 설정
                .setConverter(new GsonConverter(gson))
                .build();
    }

    //gridview에서 해당 품목 메뉴를 선택 했을때 리사이클뷰에 이미지를 띄우는 함수
    private void selectFoodImage(String category){

        MakeDietMenuFragment tempFragment = null;
        if(seletedTime.equals("morning")) tempFragment = makeDietMenuPagerAdapter.morning_fragment;
        else if(seletedTime.equals("lunch")) tempFragment = makeDietMenuPagerAdapter.lunch_fragment;
        else tempFragment = makeDietMenuPagerAdapter.dinner_fragment;

        Log.d(category, category);
        if(category.compareTo("f1")==0){
            for(int i = 0 ; i <sc_foodCategories.size();i++){
                if(sc_foodCategories.get(i).getCategory_id().compareTo(category)==0){
                    ArrayList<SC_FOOD> sc_foodArrayList =  scdbManager.getFood(sc_foodCategories.get(i));
                    recyclerView.setAdapter(new SampleRecyclerViewAdapter(sc_foodArrayList, itemLayoutRes,tempFragment));
                    break;
                }
            }

        }
        else if(category.compareTo("f2")==0){
            for(int i = 0 ; i <sc_foodCategories.size();i++){
                if(sc_foodCategories.get(i).getCategory_id().compareTo(category)==0){
                    ArrayList<SC_FOOD> sc_foodArrayList =  scdbManager.getFood(sc_foodCategories.get(i));
                    recyclerView.setAdapter(new SampleRecyclerViewAdapter(sc_foodArrayList, itemLayoutRes,tempFragment));
                    break;
                }
            }
        }else if(category.compareTo("f8")==0){
            for(int i = 0 ; i <sc_foodCategories.size();i++){
                if(sc_foodCategories.get(i).getCategory_id().compareTo(category)==0){
                    ArrayList<SC_FOOD> sc_foodArrayList =  scdbManager.getFood(sc_foodCategories.get(i));
                    recyclerView.setAdapter(new SampleRecyclerViewAdapter(sc_foodArrayList, itemLayoutRes,tempFragment));
                    break;
                }
            }
        }else{
            ArrayList<SC_FOOD> foodlist = new ArrayList<SC_FOOD>();
            for(int i = 0 ; i <sc_foodCategories.size();i++){
                if(sc_foodCategories.get(i).getCategory_id().compareTo("f1") !=0 && sc_foodCategories.get(i).getCategory_id().compareTo("f2")!=0 && sc_foodCategories.get(i).getCategory_id().compareTo("f8")!=0){
                    ArrayList<SC_FOOD> sc_foodArrayList =  scdbManager.getFood(sc_foodCategories.get(i));
                    for(int j=0;j<sc_foodArrayList.size();j++) foodlist.add(sc_foodArrayList.get(j));
                }
            }
            recyclerView.setAdapter(new SampleRecyclerViewAdapter(foodlist, itemLayoutRes,tempFragment
            ));
        }

    }




    public void onClickToFoodImageOnTable(String category){
        selectFoodImage(category);
    }


    //해당 메뉴를 선택했는 것을 표기하는 함수
    public void WhichFoodSeletedChecked(int seletedMenu){
        if(seletedTime.equals("morning")){
            if(seletedMenu==RICE){
                dietMenuMorning.setIsSelectedRice(true);
            }else if(seletedMenu==SOUP){
                dietMenuMorning.setIsSelectedSoup(true);
            }else if(seletedMenu==SIDE1){
                dietMenuMorning.setIsSelectedSide1(true);
            }else if(seletedMenu==SIDE2){
                dietMenuMorning.setIsSelectedSide2(true);
            }else if(seletedMenu==SIDE3){
                dietMenuMorning.setIsSelectedSide3(true);
            }else{
                dietMenuMorning.setIsSelectedOutDish(true);
            }
        }else if(seletedTime.equals("lunch")){
            if(seletedMenu==RICE){
                dietMenuLunch.setIsSelectedRice(true);
            }else if(seletedMenu==SOUP){
                dietMenuLunch.setIsSelectedSoup(true);
            }else if(seletedMenu==SIDE1){
                dietMenuLunch.setIsSelectedSide1(true);
            }else if(seletedMenu==SIDE2){
                dietMenuLunch.setIsSelectedSide2(true);
            }else if(seletedMenu==SIDE3){
                dietMenuLunch.setIsSelectedSide3(true);
            }else{
                dietMenuLunch.setIsSelectedOutDish(true);
            }
        }else{
            if(seletedMenu==RICE){
                dietMenuNight.setIsSelectedRice(true);
            }else if(seletedMenu==SOUP){
                dietMenuNight.setIsSelectedSoup(true);
            }else if(seletedMenu==SIDE1){
                dietMenuNight.setIsSelectedSide1(true);
            }else if(seletedMenu==SIDE2){
                dietMenuNight.setIsSelectedSide2(true);
            }else if(seletedMenu==SIDE3){
                dietMenuNight.setIsSelectedSide3(true);
            }else{
                dietMenuNight.setIsSelectedOutDish(true);
            }
        }
    }

    //메뉴가 다 선택 되었는지 안되었는지 확인 하는 함수
    public void checkAllMenu(){
        if(dietMenuNight.isCheckAllFoodMenu()&& dietMenuMorning.isCheckAllFoodMenu() && dietMenuMorning.isCheckAllFoodMenu())
            Toast.makeText(this,"오케이",Toast.LENGTH_LONG).show();
        else  Toast.makeText(this,"다 선택하셔야 합니다.",Toast.LENGTH_LONG).show();
    }

}
