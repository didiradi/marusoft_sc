package net.marusoft.marusoft_sc;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

import userinput.UserInputActivity;

public class MainActivity extends Activity {
    private Button user_btn, hitstory_btn;
    private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Initialize(); //초기화

        user_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, UserInputActivity.class);
                startActivity(intent);
            }
        });

        hitstory_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, HistoryActivity.class);
                startActivity(intent);
            }
        });

    }

    void Initialize() {
        context = this;
        user_btn = (Button) findViewById(R.id.user_btn);
        hitstory_btn = (Button) findViewById(R.id.hitstory_btn);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


}
