package util;

import android.widget.EditText;

/**
 * Created by choi on 2016-01-05.
 */
public class Static_Data {
    public static boolean isEmpty(EditText etText) { //EditText 빈칸확인
        if (etText.getText().toString().trim().length() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public static double getWannaWeight(double height, int sex_value) { //바람직한 체중
        double result = 0;
        if(sex_value == 1) { //남자
            result = (height/(double)100) * (height/(double)100) * 22;
        } else {
            result = (height/(double)100) * (height/(double)100) * 21;
        }
        return Math.round(result); //반올림
    }

    public static double getBMIValue(double weight, double height) {
        double result = 0;
        result = weight / ((height/(double)100)*(height/(double)100));
        String str = String.format("%.1f", result);
        return Double.parseDouble(str);
    }

    public static String getBMICategory(double result) { //result = getBMIValue
        String category = null;
        if(result < 18.5) {
            category = "저체중";
        } else if(result >= 18.5 && result <= 22.9) {
            category = "정상체중";
        } else if(result >= 23 && result <= 24.9) {
            category = "과체중";
        } else {
            category = "비만";
        }

        return category;
    }

    public static String getBMIcharacter(double result) {
        String character= null;
        if(result < 18.5) {
            character = "영양불량 관련 질환 발병 가능";
        } else if(result >= 18.5 && result <= 22.9) {
            character = "질환 발병 가능 낮음";
        } else if(result >= 23 && result <= 24.9) {
            character = "고혈압, 당뇨병 발병 가능";
        } else {
            character = "고혈압, 당뇨병 발병가능 높음";
        }

        return character;
    }
}
