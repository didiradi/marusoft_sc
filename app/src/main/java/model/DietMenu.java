package model;

/**
 * Created by maru on 2016-01-27.
 */

public class DietMenu {
    String time;
    private int totalkcal;
    private boolean isSelectedRice;
    private boolean isSelectedSoup;
    private boolean isSelectedSide1;
    private boolean isSelectedSide2;
    private boolean isSelectedSide3;
    private boolean isSelectedOutDish;

    public DietMenu(String time){
        this.time = time;
        totalkcal = 0;
        isSelectedRice = false;
        isSelectedSoup = false;
        isSelectedSide1 = false;
        isSelectedSide2 = false;
        isSelectedSide3 = false;
        isSelectedOutDish = false;
    }

    public boolean isCheckAllFoodMenu(){
        boolean isAllCheck = false;
        if(isSelectedRice && isSelectedSoup && isSelectedSide1
                && isSelectedSide2 && isSelectedSide3 && isSelectedOutDish) isAllCheck = true;

        return isAllCheck;
    }

    public void setIsSelectedRice(boolean isSelectedRice) {
        this.isSelectedRice = isSelectedRice;
    }

    public void setIsSelectedSoup(boolean isSelectedSoup) {
        this.isSelectedSoup = isSelectedSoup;
    }

    public void setIsSelectedSide1(boolean isSelectedSide1) {
        this.isSelectedSide1 = isSelectedSide1;
    }

    public void setIsSelectedSide2(boolean isSelectedSide2) {
        this.isSelectedSide2 = isSelectedSide2;
    }

    public void setIsSelectedSide3(boolean isSelectedSide3) {
        this.isSelectedSide3 = isSelectedSide3;
    }

    public void setIsSelectedOutDish(boolean isSelectedOutDish) {
        this.isSelectedOutDish = isSelectedOutDish;
    }

    public String getTime(){
        return time;
    }


}


