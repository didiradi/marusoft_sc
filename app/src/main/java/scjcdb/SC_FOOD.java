package scjcdb;

/**
 * Created by SEO on 2016-01-05.
 */
public class SC_FOOD {
    private String name;
    private int kcal;
    private String unit;
    private int once_eat_cal;
    private String once_eat_cal_text;
    private String img_name;
    private double natrium;
    private String category_id;
    private String category_name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getKcal() {
        return kcal;
    }

    public void setKcal(int kcal) {
        this.kcal = kcal;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public int getOnce_eat_cal() {
        return once_eat_cal;
    }

    public void setOnce_eat_cal(int once_eat_cal) {
        this.once_eat_cal = once_eat_cal;
    }

    public String getOnce_eat_cal_text() {
        return once_eat_cal_text;
    }

    public void setOnce_eat_cal_text(String once_eat_cal_text) {
        this.once_eat_cal_text = once_eat_cal_text;
    }

    public String getImg_name() {
        return img_name;
    }

    public void setImg_name(String img_name) {
        this.img_name = img_name;
    }

    public double getNatrium() {
        return natrium;
    }

    public void setNatrium(double natrium) {
        this.natrium = natrium;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public SC_FOOD(String name, int kcal, String unit, int once_eat_cal, String once_eat_cal_text, String img_name, double natrium, String category_id, String category_name) {
        this.name = name;
        this.kcal = kcal;
        this.unit = unit;
        this.once_eat_cal = once_eat_cal;
        this.once_eat_cal_text = once_eat_cal_text;
        this.img_name = img_name;
        this.natrium = natrium;
        this.category_id = category_id;
        this.category_name = category_name;
    }

    @Override
    public String toString() {
        return "SC_FOOD{" +
                "name='" + name + '\'' +
                ", kcal=" + kcal +
                ", unit='" + unit + '\'' +
                ", once_eat_cal='" + once_eat_cal + '\'' +
                ", once_eat_cal_text='" + once_eat_cal_text + '\'' +
                ", img_name='" + img_name + '\'' +
                ", natrium=" + natrium +
                ", category_id='" + category_id + '\'' +
                ", category_name='" + category_name + '\'' +
                '}';
    }
}
