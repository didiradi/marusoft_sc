package scjcdb;

/**
 * Created by SEO on 2016-01-05.
 */
public class UploadFoodItem {
    String name;
    int kcal;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getKcal() {
        return kcal;
    }

    public void setKcal(int kcal) {
        this.kcal = kcal;
    }

    public UploadFoodItem(String name, int kcal) {
        this.name = name;
        this.kcal = kcal;
    }
}
