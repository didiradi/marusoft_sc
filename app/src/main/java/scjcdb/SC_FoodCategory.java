package scjcdb;

/**
 * Created by SEO on 2016-01-05.
 */
public class SC_FoodCategory {
        private String category_id;
        private String name;
        private String unit;

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public SC_FoodCategory(String category_id, String name, String unit) {
        this.category_id = category_id;
        this.name = name;
        this.unit = unit;
    }

    @Override
    public String toString() {
        return "SC_FoodCategory{" +
                "category_id='" + category_id + '\'' +
                ", name='" + name + '\'' +
                ", unit='" + unit + '\'' +
                '}';
    }
}
