package scjcdb;

/**
 * Created by SEO on 2016-01-05.
 */
public class SCWannaWeight {
    public static final int MAN_ID = 1;
    public static final int WOMAN_ID = 2;

    private int height;
    private  int weight;
    private  int gender_int;
    private  String gender_str;

    public SCWannaWeight(int height, int weight,int gender_int) {
        this.gender_int = gender_int;
        this.height = height;
        this.weight = weight;
        if(gender_int==1){
            gender_str = "남자";
        }else if(gender_int==2){
            gender_str = "여자";
        }
    }


    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getGender_int() {
        return gender_int;
    }

    public void setGender_int(int gender_int) {
        this.gender_int = gender_int;
    }

    public String getGender_str() {
        return gender_str;
    }

    public void setGender_str(String gender_str) {
        this.gender_str = gender_str;
    }


    @Override
    public String toString() {
        return "SCWannaWeight{" +
                "height=" + height +
                ", weight=" + weight +
                ", gender_int=" + gender_int +
                ", gender_str='" + gender_str + '\'' +
                '}';
    }
}
