package scjcdb;

/**
 * Created by SEO on 2016-01-05.
 */
public class SCBMI_FROM_CAL {
    private int weight;
    private int easy_cal;
    private int normal_cal;
    private int hard_cal;

    public SCBMI_FROM_CAL(int weight, int easy_cal, int normal_cal, int hard_cal) {
        this.weight = weight;
        this.easy_cal = easy_cal;
        this.normal_cal = normal_cal;
        this.hard_cal = hard_cal;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getEasy_cal() {
        return easy_cal;
    }

    public void setEasy_cal(int easy_cal) {
        this.easy_cal = easy_cal;
    }

    public int getNormal_cal() {
        return normal_cal;
    }

    public void setNormal_cal(int normal_cal) {
        this.normal_cal = normal_cal;
    }

    public int getHard_cal() {
        return hard_cal;
    }

    public void setHard_cal(int hard_cal) {
        this.hard_cal = hard_cal;
    }


    @Override
    public String toString() {
        return "SCBMI_FROM_CAL{" +
                "weight=" + weight +
                ", easy_cal=" + easy_cal +
                ", normal_cal=" + normal_cal +
                ", hard_cal=" + hard_cal +
                '}';
    }
}
