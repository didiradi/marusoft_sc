package scjcdb;

import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by Henen on 2016-01-05.
 */

public class SCDBManager {
private Context context;
    private SQLiteDatabase db;
    private SCDB_Helper scdb_helper;
    public static final String PACKAGE_NAME = "net.marusoft.marusoft_sc";
    public static final String DB_NAME = "scjs.db";



    public SCDBManager(Context context) {
        this.context = context;
       if(loadDBFromAsset(context)==true){
           this.scdb_helper = new SCDB_Helper(context);
       }else{
            //실패
       }
    }



    public SCWannaWeight getWannaWeight(int height,int gender_id){
        db = scdb_helper.getReadableDatabase();
        Cursor cursor;
        String sql ="Select weight from WANNA_WEIGHT_TB where height = '"+height+"' and gender = '"+ gender_id+"'";
        cursor = db.rawQuery(sql,null);
        int weight = 0;
        SCWannaWeight scWannaWeight = null;
        if(cursor.getCount()==0){

        }else{
            if(cursor.moveToFirst()){
                weight  = cursor.getInt(cursor.getColumnIndex("weight"));
                scWannaWeight = new SCWannaWeight(height,weight,gender_id);
            }
        }
        return scWannaWeight;
    }

    public ArrayList<SCWannaWeight> getAllWannaTable(int gender_id){

        db = scdb_helper.getReadableDatabase();
        Cursor cursor;
        String sql ="Select height,weight from WANNA_WEIGHT_TB where gender = '"+ gender_id+"'";
        cursor = db.rawQuery(sql,null);
        int weight = 0;
        int height = 0;
        ArrayList<SCWannaWeight> scWannaWeightArrayList = new ArrayList<SCWannaWeight>();
        if(cursor.getCount()==0){

        }else{
            if(cursor.moveToFirst()){
                do{
                    weight  = cursor.getInt(cursor.getColumnIndex("weight"));
                    height  = cursor.getInt(cursor.getColumnIndex("height"));
                    scWannaWeightArrayList.add(new SCWannaWeight(height,weight,gender_id));
                }while (cursor.moveToNext());

            }
        }
        return scWannaWeightArrayList;
    }




    /*
        비만도가 저체중일때, 정상일때, 과체중일때, 과체중 비만일때의 상태를 체크하여
        바람직한 체중 및 과체중저체중 상태를 넣으면 가벼운활동,보통활동,힘든 활동 열량이나옴
        //bmi_state는 SCBMI에 static으로 있음

     */
    public SCBMI_FROM_CAL getSCBMICAL(int wanna_weight,int bmi_state){
        db = scdb_helper.getReadableDatabase();
        Cursor cursor;
        String sql = null;
        switch (bmi_state){
            case SCBMI.UNDER_WEIHGT :
                sql ="Select easy_cal,normal_cal,hard_cal from UNDER_WEIGHT_CAL_TB  where weight = '"+ wanna_weight+"'";
                break;

            case SCBMI.NORMAL_WEIGHT :
                sql ="Select easy_cal,normal_cal,hard_cal from NORMAL_WEIGHT_CAL_TB  where weight = '"+ wanna_weight+"'";
                break;

            case SCBMI.OVER_WEIGHT :
                sql ="Select easy_cal,normal_cal,hard_cal from OVER_WEIGHT_CAL_TB  where weight = '"+ wanna_weight+"'";
                break;

            case SCBMI.OVER_OBESITY_WEIGHT :
                sql ="Select easy_cal,normal_cal,hard_cal from OVER_OBESTIY_WEIGHT_CAL  where weight = '"+ wanna_weight+"'";
                break;
        }

        cursor = db.rawQuery(sql,null);
        int easy_cal = 0;
        int normal_cal = 0;
        int hard_cal = 0;


        SCBMI_FROM_CAL scbmi_from_cal = null;
        if(cursor.getCount()==0){

        }else{
            if(cursor.moveToFirst()){
                easy_cal  = cursor.getInt(cursor.getColumnIndex("easy_cal"));
                normal_cal  = cursor.getInt(cursor.getColumnIndex("normal_cal"));
                hard_cal  = cursor.getInt(cursor.getColumnIndex("hard_cal"));
                scbmi_from_cal = new SCBMI_FROM_CAL(wanna_weight,easy_cal,normal_cal,hard_cal);
            }
        }
        return scbmi_from_cal;
    }

    //비만도별 활동량 칼로리 소모 테이블
    public ArrayList<BMI_ACTIVITY_CAL> getBMIActivity_CAL_TB(){
        db = scdb_helper.getReadableDatabase();
        Cursor cursor;
        String sql = "select activity,activity_detail,under_weight,normal_weight,over_weight from BMI_ACTIVITY_CAL";
        cursor = db.rawQuery(sql,null);
        ArrayList<BMI_ACTIVITY_CAL> bmi_activity_calArrayList = new ArrayList<BMI_ACTIVITY_CAL>();

         String activity;
         String activity_detail;
         int under_weight_cal;
         int normal_weight_cal;
         int over_weight_cal;

        if(cursor.getCount()==0){

        }else{
            if(cursor.moveToFirst()){
                do{
                    activity  = cursor.getString(cursor.getColumnIndex("activity"));
                    activity_detail  = cursor.getString(cursor.getColumnIndex("activity_detail"));
                    under_weight_cal  = cursor.getInt(cursor.getColumnIndex("under_weight"));
                    normal_weight_cal  = cursor.getInt(cursor.getColumnIndex("normal_weight"));
                    over_weight_cal  = cursor.getInt(cursor.getColumnIndex("over_weight"));
                    bmi_activity_calArrayList.add(new BMI_ACTIVITY_CAL(activity,activity_detail,under_weight_cal,normal_weight_cal,over_weight_cal));
                }while (cursor.moveToNext());

            }
        }
        return bmi_activity_calArrayList;
    }

    public ArrayList<SC_FoodCategory> getSCFoodCategory(){
        db = scdb_helper.getReadableDatabase();
        Cursor cursor;
        String sql = "Select name,category_id,unit from FOOD_CATEGORY";
        cursor = db.rawQuery(sql,null);
        ArrayList<SC_FoodCategory>foodCategoryArrayList = new ArrayList<SC_FoodCategory>();

        String name = "";
        String category_id = "";
        String unit = "";

        if(cursor.getCount()==0){

        }else{
            if(cursor.moveToFirst()){
                do{
                    name  = cursor.getString(cursor.getColumnIndex("name"));
                    category_id  = cursor.getString(cursor.getColumnIndex("category_id"));
                    unit  = cursor.getString(cursor.getColumnIndex("unit"));
                    foodCategoryArrayList.add(new SC_FoodCategory(category_id,name,unit));
                }while (cursor.moveToNext());

            }
        }
        return foodCategoryArrayList;
    }

    public ArrayList<SC_FOOD> getFood(SC_FoodCategory food_category){

        db = scdb_helper.getReadableDatabase();
        Cursor cursor;
        String sql = "Select name,once_eat_cal,once_eat_cal_text,kcal,natrium,img_name,category from FOOD_TB where category ='"+food_category.getCategory_id()+"'";
        cursor = db.rawQuery(sql, null);
        ArrayList<SC_FOOD> sc_foodArrayList = new ArrayList<SC_FOOD>();

         String name;
         int kcal;
         String unit = food_category.getUnit();
         String category_id = food_category.getCategory_id();
         String category_name = food_category.getName();
         int once_eat_cal;
         String once_eat_cal_text;
         String img_name;
         double natrium;

        if(cursor.getCount()==0){

        }else{
            if(cursor.moveToFirst()){
                do{
                    name  = cursor.getString(cursor.getColumnIndex("name"));
                    kcal  = cursor.getInt(cursor.getColumnIndex("kcal"));
                    once_eat_cal  = cursor.getInt(cursor.getColumnIndex("once_eat_cal"));
                    once_eat_cal_text  = cursor.getString(cursor.getColumnIndex("once_eat_cal_text"));
                    img_name  = cursor.getString(cursor.getColumnIndex("img_name"));
                    natrium  = cursor.getDouble(cursor.getColumnIndex("natrium"));
                    sc_foodArrayList.add(new SC_FOOD(name,kcal,unit,once_eat_cal,once_eat_cal_text,img_name,natrium,category_id,category_name));
                }while (cursor.moveToNext());

            }
        }
        return sc_foodArrayList;
    }

    public boolean loadDBFromAsset(Context context){
        boolean result = false;
        try {
            boolean bResult = isCheckDB(context);  // DB가 있는지?
            Log.d("MiniApp", "DB Check="+bResult);
            if(!bResult){   // DB가 없으면 복사
                copyDB(context);
                result = true;
            }else{
                result = true;
            }
        } catch (Exception e) {
            result = false;
        }
        return result;
    }

    // DB가 있나 체크하기
    public boolean isCheckDB(Context mContext){
        String filePath = "/data/data/" + PACKAGE_NAME + "/databases/" + DB_NAME;
        File file = new File(filePath);

        if (file.exists()) {
            return true;
        }

        return false;

    }
    // DB를 복사하기
// assets의 /db/xxxx.db 파일을 설치된 프로그램의 내부 DB공간으로 복사하기
    public void copyDB(Context mContext){
        Log.d("MiniApp", "copyDB");
        AssetManager manager = mContext.getAssets();
        String folderPath = "/data/data/" + PACKAGE_NAME + "/databases";
        String filePath = "/data/data/" + PACKAGE_NAME + "/databases/" + DB_NAME;
        File folder = new File(folderPath);
        File file = new File(filePath);

        FileOutputStream fos = null;
        BufferedOutputStream bos = null;
        try {
            InputStream is = manager.open("db/" + DB_NAME);
            BufferedInputStream bis = new BufferedInputStream(is);

            if (folder.exists()) {
            }else{
                folder.mkdirs();
            }


            if (file.exists()) {
                file.delete();
                file.createNewFile();
            }

            fos = new FileOutputStream(file);
            bos = new BufferedOutputStream(fos);
            int read = -1;
            byte[] buffer = new byte[1024];
            while ((read = bis.read(buffer, 0, 1024)) != -1) {
                bos.write(buffer, 0, read);
            }

            bos.flush();

            bos.close();
            fos.close();
            bis.close();
            is.close();

        } catch (IOException e) {
            Log.e("ErrorMessage : ", e.getMessage());
        }
    }

}



