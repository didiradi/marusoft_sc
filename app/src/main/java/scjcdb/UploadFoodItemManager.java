package scjcdb;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by SEO on 2016-01-05.
 */
public class UploadFoodItemManager {

    private ArrayList<UploadFoodItem> morning_arrayList;
    private ArrayList<UploadFoodItem> lunch_arrayList;
    private ArrayList<UploadFoodItem> dinner_arrayList;

    public UploadFoodItemManager() {
        this.morning_arrayList = new ArrayList<UploadFoodItem>();
        this.lunch_arrayList = new ArrayList<UploadFoodItem>();
        this.dinner_arrayList = new ArrayList<UploadFoodItem>();
    }

    public void morningAdd(SC_FOOD sc_food){
        morning_arrayList.add(new UploadFoodItem(sc_food.getName(),sc_food.getKcal()));
    }
    public void lunch_arrayList(SC_FOOD sc_food){
        lunch_arrayList.add(new UploadFoodItem(sc_food.getName(),sc_food.getKcal()));
    }
    public void dinner_arrayList(SC_FOOD sc_food){
        dinner_arrayList.add(new UploadFoodItem(sc_food.getName(),sc_food.getKcal()));
    }

    public JSONObject makeJson(){
        Gson gson = new Gson();
        JSONObject resultObject = new JSONObject();
            try {
               // resultObject.put("morning","aa");
                JSONArray morning_jsonArray = new JSONArray();
                JSONArray lunch_jsonArray = new JSONArray();
                JSONArray dinner_jsonArray = new JSONArray();
                for(int i = 0 ; i< morning_arrayList.size();i++){
                    JSONObject morning_object = new JSONObject();
                    morning_object.put("name",morning_arrayList.get(i).getName());
                    morning_object.put("kcal",morning_arrayList.get(i).getKcal());
                    morning_jsonArray.put(morning_object);
                }
                for(int i = 0 ; i< lunch_arrayList.size();i++){
                    JSONObject lunch_object = new JSONObject();
                    lunch_object.put("name",lunch_arrayList.get(i).getName());
                    lunch_object.put("kcal",lunch_arrayList.get(i).getKcal());
                    lunch_jsonArray.put(lunch_object);
                }
                for(int i = 0 ; i< dinner_arrayList.size();i++){
                    JSONObject dinner_object = new JSONObject();
                    dinner_object.put("name",dinner_arrayList.get(i).getName());
                    dinner_object.put("kcal", dinner_arrayList.get(i).getKcal());
                    dinner_jsonArray.put(dinner_object);
                }
                resultObject.put("morning",morning_jsonArray);
                resultObject.put("lunch",lunch_jsonArray);
                resultObject.put("dinner",dinner_jsonArray);


            } catch (JSONException e) {
                e.printStackTrace();
            }
        return resultObject;
        }

    }




