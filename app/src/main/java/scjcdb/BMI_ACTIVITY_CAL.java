package scjcdb;

/**
 * Created by SEO on 2016-01-05.
 */
public class BMI_ACTIVITY_CAL {

    private String activity;
    private String activity_detail;
    private int under_weight_cal;
    private int normal_weight_cal;
    private int over_weight_cal;


    public BMI_ACTIVITY_CAL(String activity, String activity_detail, int under_weight_cal, int normal_weight_cal, int over_weight_cal) {
        this.activity = activity;
        this.activity_detail = activity_detail;
        this.under_weight_cal = under_weight_cal;
        this.normal_weight_cal = normal_weight_cal;
        this.over_weight_cal = over_weight_cal;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getActivity_detail() {
        return activity_detail;
    }

    public void setActivity_detail(String activity_detail) {
        this.activity_detail = activity_detail;
    }

    public int getUnder_weight_cal() {
        return under_weight_cal;
    }

    public void setUnder_weight_cal(int under_weight_cal) {
        this.under_weight_cal = under_weight_cal;
    }

    public int getNormal_weight_cal() {
        return normal_weight_cal;
    }

    public void setNormal_weight_cal(int normal_weight_cal) {
        this.normal_weight_cal = normal_weight_cal;
    }

    public int getOver_weight_cal() {
        return over_weight_cal;
    }

    public void setOver_weight_cal(int over_weight_cal) {
        this.over_weight_cal = over_weight_cal;
    }


    @Override
    public String toString() {
        return "BMI_ACTIVITY_CAL{" +
                "activity='" + activity + '\'' +
                ", activity_detail='" + activity_detail + '\'' +
                ", under_weight_cal=" + under_weight_cal +
                ", normal_weight_cal=" + normal_weight_cal +
                ", over_weight_cal=" + over_weight_cal +
                '}';
    }
}
