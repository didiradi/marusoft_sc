package scjcdb;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by SEO on 2016-01-05.
 */
public class SCDB_Helper extends SQLiteOpenHelper{
    private Context context;

    public SCDB_Helper(Context context) {
        super(context, SCDBManager.DB_NAME, null, 1);  // 제일 마지막 인자 : 버젼, 만약 버젼이 높아지면 onUpgrade를 수행한다.
        this.context = context;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        // 원래 여기에 create 문이 들어가야하나 기존에 있는 DB를 사용하므로 생략
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // 기존에 있는 DB를 사용하므로 생략
    }


}
