package userinput;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by MARUSOFT-CHOI on 2016-01-05.
 */
public class UserInputAdapter extends FragmentStatePagerAdapter {
    public static final int NUM_PAGES = 3;

    public UserInputAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        if(position == 0) {
            Fragment fragment = new Fragment_UserInput_1();
            /*Bundle args = new Bundle();
            args.putInt(ARGS_POSITION, position);
            fragment.setArguments(args);*/
            return fragment;
        } else if(position == 1) {
            Fragment fragment = new Fragment_UserInput_2();
            return fragment;
        } else {
            Fragment fragment = new Fragment_UserInput_3();
            return fragment;
        }
    }

    @Override
    public int getCount() {
        return NUM_PAGES;
    }
}
