package userinput;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.viewpagerindicator.CirclePageIndicator;

import net.marusoft.marusoft_sc.R;

import util.CustomCircleIndicater;
import util.CustomViewPager;

/**
 * Created by MARUSOFT-CHOI on 2016-01-05.
 */
public class UserInputActivity extends FragmentActivity {

         private CustomCircleIndicater mCirclePageIndicator;
        public static CustomViewPager mViewPager;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_userinput);

            mViewPager = (CustomViewPager) findViewById(R.id.pager);

            mCirclePageIndicator = (CustomCircleIndicater) findViewById(R.id.indicator);
            mCirclePageIndicator.setCircleExtraSpace(130);
//            mCirclePageIndicator.setFillColor(Color.parseColor("#F66B09"));

            UserInputAdapter adapter =
                    new UserInputAdapter(getSupportFragmentManager());
            mViewPager.setAdapter(adapter);
            mViewPager.setPagingDisabled(); //페이지 스크롤 막기

            mCirclePageIndicator.setViewPager(mViewPager);

           // mCirclePageIndicator.getItem

    }
}
