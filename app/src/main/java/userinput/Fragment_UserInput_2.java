package userinput;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import net.marusoft.marusoft_sc.R;

import util.RbPreference;
import util.Static_Data;

/**
 * Created by MARUSOFT-CHOI on 2016-01-05.
 */
public class Fragment_UserInput_2 extends Fragment {
    private TextView wanna_tv, bmi_tv, result_tv, character_tv;
    private Button pre_btn, next_btn;
    private RbPreference pref = null;
    private Button character_btn;
    private AlertDialog character_dialog;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_userinput_2, container, false);

        Initialize(v);

        pre_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserInputActivity.mViewPager.setCurrentItem(0);
            }
        });

        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserInputActivity.mViewPager.setCurrentItem(2);
            }
        });
        character_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                character_dialog.show();
            }
        });
        return v;
    }

    void Initialize(View v) {
        wanna_tv = (TextView) v.findViewById(R.id.wanna_tv);
        bmi_tv = (TextView) v.findViewById(R.id.bmi_tv);
        result_tv = (TextView) v.findViewById(R.id.result_tv);
        character_tv = (TextView) v.findViewById(R.id.character_tv);
        pre_btn = (Button) v.findViewById(R.id.pre_btn);
        next_btn = (Button) v.findViewById(R.id.next_btn);
        character_btn  = (Button)v.findViewById(R.id.character_btn);


        AlertDialog.Builder ab = new AlertDialog.Builder(getActivity(),android.R.style.Theme_Material_Light_Dialog_Alert);

        ab.setView(R.layout.character_dialog);
        character_dialog =   ab.create();


    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (isVisibleToUser) {
            // 보인다.
            pref = new RbPreference(getActivity());
            //wannaWeight = scdbManager.getWannaWeight(Integer.parseInt(pref.getValue(RbPreference.USER_STATURE, "")), pref.getValue(RbPreference.USER_SEX, 1));

            wanna_tv.setText(Static_Data.getWannaWeight(Double.parseDouble(pref.getValue(RbPreference.USER_STATURE, "")), pref.getValue(RbPreference.USER_SEX, 1))+""); //바람직한 체중
            double BMI_result = Static_Data.getBMIValue(Double.parseDouble(pref.getValue(RbPreference.USER_WEIGHT, "")), Double.parseDouble(pref.getValue(RbPreference.USER_STATURE, "")));
            bmi_tv.setText(BMI_result+"");
            result_tv.setText("" + Static_Data.getBMICategory(BMI_result));
            character_tv.setText("" + Static_Data.getBMIcharacter(BMI_result));
        } else {
            // 안보인다.
        }
        super.setUserVisibleHint(isVisibleToUser);
    }


}
