package userinput;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import net.marusoft.marusoft_sc.R;

import util.RbPreference;
import util.Static_Data;

/**
 * Created by MARUSOFT-CHOI on 2016-01-05.
 */
public class Fragment_UserInput_1 extends Fragment {
    private Button next_btn;
    private EditText name_et, stature_et, weight_et;
    private Spinner gender_spinner;
    private RadioGroup radiogroup_1;
    private int sex_value;
    String gender_value [] ={"남자","여자"};
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_userinput_1, container, false);

        Initialize(v);
        gender_spinner.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, gender_value));

        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Static_Data.isEmpty(name_et) || Static_Data.isEmpty(stature_et) || Static_Data.isEmpty(weight_et)) {
                    Toast.makeText(getActivity(), "빈칸을 확인해 주세요", Toast.LENGTH_SHORT).show();
                } else {
                    RbPreference pref = new RbPreference(getActivity());
                    pref.put(RbPreference.USER_NAME, name_et.getText().toString().trim()); //이름 put
                    if (gender_spinner.getSelectedItemPosition() == 0) {
                        sex_value = 1; //남자
                    } else {
                        sex_value = 2; //여자
                    }
                    pref.put(RbPreference.USER_SEX, sex_value);
                    pref.put(RbPreference.USER_STATURE, stature_et.getText().toString().trim());
                    pref.put(RbPreference.USER_WEIGHT, weight_et.getText().toString().trim());

                    UserInputActivity.mViewPager.setCurrentItem(1);
                }
            }
        });

        gender_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0 :
                        Toast.makeText(getActivity(), "남자를 선택하였습니다.", Toast.LENGTH_SHORT).show();
                        break;
                    case 1 :
                        Toast.makeText(getActivity(), "여자를 선택하였습니다.", Toast.LENGTH_SHORT).show();
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        return v;
    }

    void Initialize(View v) {
        next_btn = (Button) v.findViewById(R.id.next_btn);
        name_et = (EditText) v.findViewById(R.id.name_et);
        stature_et = (EditText) v.findViewById(R.id.stature_et);
        weight_et = (EditText) v.findViewById(R.id.weight_et);
        gender_spinner  = (Spinner)v.findViewById(R.id.gender_spinner);
    }
}
