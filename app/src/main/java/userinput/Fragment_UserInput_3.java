package userinput;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import net.marusoft.marusoft_sc.MakeDietmenuFormineActivity;
import net.marusoft.marusoft_sc.R;

import java.util.ArrayList;

import scjcdb.BMI_ACTIVITY_CAL;
import scjcdb.SCBMI;
import scjcdb.SCBMI_FROM_CAL;
import scjcdb.SCDBManager;
import util.RbPreference;
import util.Static_Data;

/**
 * Created by choi on 2016-01-05.
 */
public class Fragment_UserInput_3 extends Fragment {
    private Spinner spinner;
    private TextView today_kcal_tv;
    private RbPreference pref;
    private SCDBManager scdbManager;
    private ArrayList<BMI_ACTIVITY_CAL> bmi_activity_cals = new ArrayList<>();
    private ArrayList<String> spinner_items = new ArrayList<>();
    private SCBMI_FROM_CAL scbmi_from_cal;
    private int scbmi_value = 0;
    private double wanna_weight = 0;
    private Button pre_btn, next_btn;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_userinput_3, container, false);

        Initialize(v);

        pre_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserInputActivity.mViewPager.setCurrentItem(1);
            }
        });

        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
                Intent intent = new Intent(getActivity(), MakeDietmenuFormineActivity.class);
                startActivity(intent);


            }
        });

        return v;
    }

    void Initialize(View v) {
        spinner = (Spinner) v.findViewById(R.id.spinner);
        today_kcal_tv = (TextView) v.findViewById(R.id.today_kcal_tv);
        pre_btn = (Button) v.findViewById(R.id.pre_btn);
        next_btn = (Button) v.findViewById(R.id.next_btn);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (isVisibleToUser) {
            // 보인다.
            pref = new RbPreference(getActivity());
            scdbManager = new SCDBManager(getActivity());
            bmi_activity_cals = scdbManager.getBMIActivity_CAL_TB();
            spinner_items.clear();
            for(int i=0; i<bmi_activity_cals.size(); i++) {
                spinner_items.add(bmi_activity_cals.get(i).getActivity() + "\n" + bmi_activity_cals.get(i).getActivity_detail());
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                    R.layout.spinner_item, spinner_items);

            spinner.setAdapter(adapter);

            wanna_weight = Static_Data.getWannaWeight(Double.parseDouble(pref.getValue(RbPreference.USER_STATURE, "")), pref.getValue(RbPreference.USER_SEX, 1));
            double BMI_result = Static_Data.getBMIValue(Double.parseDouble(pref.getValue(RbPreference.USER_WEIGHT, "")), Double.parseDouble(pref.getValue(RbPreference.USER_STATURE, "")));

            if(Static_Data.getBMICategory(BMI_result).trim().equals("저체중")) {
                scbmi_value = SCBMI.UNDER_WEIHGT;
            } else if(Static_Data.getBMICategory(BMI_result).trim().equals("정상체중")) {
                scbmi_value = SCBMI.NORMAL_WEIGHT;
            } else if(Static_Data.getBMICategory(BMI_result).trim().equals("과체중")) {
                scbmi_value = SCBMI.OVER_WEIGHT;
            } else {
                scbmi_value = SCBMI.OVER_OBESITY_WEIGHT;
            }


            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                    int today_kcal = 0;
                    //scbmi_from_cal = scdbManager.getSCBMICAL((int) Math.floor(wanna_weight), scbmi_value);
                    if (position == 0) { //가벼운활동
                        if(scbmi_value == 1) { //저체중
                            today_kcal = (int)wanna_weight * 35;
                        } else if(scbmi_value == 2) { //보통체중
                            today_kcal = (int)wanna_weight * 30;
                        } else { //과체중, 비만
                            today_kcal = (int)wanna_weight * 25;
                        }
                        //today_kcal = scbmi_from_cal.getEasy_cal();
                    } else if (position == 1) { //보통활동
                        if(scbmi_value == 1) { //저체중
                            today_kcal = (int)wanna_weight * 40;
                        } else if(scbmi_value == 2) { //보통체중
                            today_kcal = (int)wanna_weight * 35;
                        } else { //과체중, 비만
                            today_kcal = (int)wanna_weight * 30;
                        }
                        //today_kcal = scbmi_from_cal.getNormal_cal();
                    } else { //힘든활동
                        if(scbmi_value == 1) { //저체중
                            today_kcal = (int)wanna_weight * 45;
                        } else if(scbmi_value == 2) { //보통체중
                            today_kcal = (int)wanna_weight * 40;
                        } else { //과체중, 비만
                            today_kcal = (int)wanna_weight * 35;
                        }
                        //today_kcal = scbmi_from_cal.getHard_cal();
                    }
                    pref.put(RbPreference.USER_TODAY_KCAL, today_kcal);

                    today_kcal_tv.setText("" + today_kcal);

                    Log.v("todaykcal", "todaykcal : " + pref.getValue(RbPreference.USER_TODAY_KCAL, 0));
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

        } else {
            // 안보인다.
        }
        super.setUserVisibleHint(isVisibleToUser);
    }
}
